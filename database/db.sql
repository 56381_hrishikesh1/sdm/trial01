CREATE TABLE Emp(empid INT PRIMARY KEY AUTO_INCREMENT, name VARCHAR(50), salary DOUBLE, age INT);

INSERT INTO Emp(name, salary, age) VALUES ("Hrishi", 95000, 22);
INSERT INTO Emp(name, salary, age) VALUES ("Jeet", 90000, 23);
INSERT INTO Emp(name, salary, age) VALUES ("DD", 85000, 23);
INSERT INTO Emp(name, salary, age) VALUES ("Gauri", 75000, 23);