const express = require('express')
const cors = require('cors')
const employeeRouter = require('./routes/employee')

const app = express()

app.use(cors('*'))
app.use(express.json())

app.use('/employee', employeeRouter)

app.listen(4000, '0.0.0.0' , ()=>{
    console.log('Server Started on Port 4000')
})