const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.get('/getEmployees', (request,response)=>{
    const statement = `SELECT * FROM Emp`
    db.execute(statement, (error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

router.post('/addEmployee', (request,response)=>{
    const { name, salary, age} = request.body
    const statement = `INSERT INTO Emp VALUES (DEFAULT, '${name}', ${salary}, ${age})`
    db.execute(statement, (error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

router.put('/updateEmployee/:id', (request,response)=>{
    const { id } = request.params
    const { name, salary, age} = request.body
    const statement = `
    UPDATE Emp
    SET
    name = '${name}',
    salary = ${salary},
    age = ${age}
    WHERE empid = ${id}
    `
    db.execute(statement, (error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

router.delete('/deleteEmployee/:id', (request,response)=>{
    const { id } = request.params
    const statement = `DELETE FROM Emp WHERE empid = ${id} `
    db.execute(statement, (error,data)=>{
        response.send(utils.createResult(error,data))
    })
})


module.exports = router