const mysql = require('mysql2')

const pool = mysql.createPool({
    host: 'database',
    user: 'root',
    password: 'root',
    database: 'sdmLabExam',
    port: 3306,
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
})

module.exports = pool